# HOA Code Test #

Instructions (these are also in the PDF the candidate receives)
Create a branch from master to work on and give it a name (your name for example) so we can ID it. Clone the branch to your local machine, you should have everything you need in the repo. If you can't find something, spot a mistake (very likely) then please email: john@hoa.org.uk

The test is split into two parts, please don't use any CSS frameworks or JQuery (no JS DOM manipulation is required for this test).

## 1. Front-end / Wordpress test ##
Create an in-page UI element that can be inserted into content via the wordpress editor using a shortcode. The UI element should be based on the supplied visuals in the PDF (Desktop and Mobile). HTML markup and an image are provided in the "images" directory and "/shortcode/index.html" . Add classes to the markup ( found in /shortcode/index.html ) and then link the provided CSS file ( /shortcode/CSS/styles.css ) to the HTML. Styles should be responsive and cater for mobile and desktop. When you are happy with your styling copy your elements HTML (now with added classes) and paste into functions.php (  /shortcode/functions.php ). Wrap the markup in a PHP function that has a WP shortcode that could be used by editorial staff ( non technical ). Web font for the headline and button is Montserrat (Google web font). Please use a sans-serif system font for the paragraph tag.
Bonus points for adding a slight hover effect for desktop users.

## 2. PHP / Webhook test ##
Create a web form in the provided php file ( /webform/form.php ). The form should have input fields for “firstname”, “lastname”, “email”, “telephone” and “comments”. Give each input an appropriate type. Make sure your form is well formed (labels etc). No need for styling or security features (regex checking etc). All inputs should be required apart from “comments”. When the user hits submit the form sould send the forms input values to the following webhook  https://hooks.zapier.com/hooks/catch/5444715/byvwqr5/ 
Once complete fill out the form (use a local PHP server if possible). Use your own email address in the form and if all goes to plan you should get an automated response email into your inbox (check your spam folder).
If you don’t get the email don’t worry too much just send over the completed PHP for us to check.

Please don’t spend too long on these tasks, we just want to see you have a basic understanding of core concepts. 

When your done, commit your changes and push back to your working branch or just zip the directory back up and email to john@hoa.org.uk . 
